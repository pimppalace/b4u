document.addEventListener('DOMContentLoaded', function() {
    document.getElementById("loadersito").style.display = "none";

    if(localStorage.getItem('usuarioLS') !== undefined && localStorage.getItem('correoLS')!== undefined && //revisa que si en local sotrage el usuario y correo estan guardados
        localStorage.getItem('usuarioLS') && localStorage.getItem('correoLS')){                            //que los guarde en las variables "correo" y "usuarioN" e invoque el metodo mandarDatos utilizando
                                                                                                           //las variables como parametros
      var correo= {
          "correo" :  localStorage.getItem('correoLS')
      }

      var usuarioN = localStorage.getItem('usuarioLS');

      mandarDatos(correo,usuarioN);
    }


     var boton = document.getElementById('botonLog');                           //botoncillo que invoca el metodo comprobar datos
     boton.addEventListener('click', function() {
       comprobarDatos();

     });
});

function comprobarDatos(){                                                      //Este metodo sirve para comprobar datos
  var baseurl = 'http://198.101.238.125:3000/api/login';                        //Url de la api

  var correoid = document.getElementById("correologin").value;                  //variable que saca los atos del campo de texto de correo
  var usuarioN = document.getElementById("unameID").value;                      //lo mismo pero para el usuario

  console.log(usuarioN);
  if(usuarioN==""){                                                             //checa que no este vacio el campo
    alert("¡Usuario vacio!")
    return null
  }
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;             //aqui checa que el correo sea valido
  if(correoid.match(mailformat))
  {
  //document.form1.text1.focus();
  }
  else
  {
  alert("¡Haz ingresado un correo electronico invalido!");
  //document.form1.text1.focus();
  return false;
  }

  var correo ={                                                                 //guarda el correo
    "correo" : correoid
  };
  console.log(correo);
  mandarDatos(correo,usuarioN);                                                 //invoca el metodo siguiente para validar los datos y mandarlos
}
//-------

function mandarDatos(correo,usuarioN){                            //Metodo para mandar datos

  document.getElementById("loadersito").style.display = "block";  //quita el incono de loading
  var baseurl = 'http://198.101.238.125:3000/api/login';          // url para loguearse
  fetch(baseurl , {                                               //inicia el fect utilizando el url
     method: 'POST',                                              // usa un POST
     headers: {                                                   //agrega los headers necesarios para el token
    'Content-Type': 'application/json',
    "alg": "HS256",
    "typ": "JWT"
  },
     body: JSON.stringify(correo)                                 //correo ingresado (lo saco del local storage si ya se habia logueado antes)
  })
  .then(function(response) {                                      //aqui se manda
     if(response.ok) {
         return response.text()
     } else {
         throw "Error en la llamada Ajax";                        //si hay algun error con el servidor te avisa
         alert('Servidor abajo.');
     }
  })
  .then(function(texto) {                                         //token
    var usuarioLC = document.getElementById("unameID").value;     //id que saca del usuario
    console.log(usuarioLC);
    localStorage.setItem('usuarioLS', usuarioN);                  //guarda en el local sotrage el usuario
    localStorage.setItem('correoLS', correo['correo']);           //igual con el correo

    localStorage.setItem('token', texto);                         //y el token
    document.location.href = '/popup.html';                       //y al final pasa a la parte principal (si todo salio bien)
    console.log(texto);
  })
  .catch(function(err) {
     console.log(err);
  });
}
