// event listener for the button inside popup window
document.addEventListener('DOMContentLoaded', function() {
        addURL();
});

// add the URL inside the popup-window's <div>
function addURL() {
// store info in the the queryInfo object as per: https://developer.chrome.com/extensions/tabs#method-query
    var queryInfo = {
    currentWindow: true,
    active: true
    };

    chrome.tabs.query(queryInfo, function(tabs) {
    // tabs is an array so fetch the first (and only) object-elemnt in tab
    // put URL propery of tab in another variable as per: https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tabs[0].url;

    // put the content into the popup-window's <div>
    document.getElementById("hipervinculo").innerHTML = url;
    });
}
//basicamente lo que hace esto es checar la pestaña abierta actual e insertar su url en el elemento con id "hipervinculo"
