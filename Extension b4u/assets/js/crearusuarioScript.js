document.addEventListener('DOMContentLoaded', function() {

     var boton = document.getElementById('agregarUsu');
     boton.addEventListener('click', function() {
       crearUsuario();
       alert('Usuario creado.')
       document.location.href = '/loginP.html';
     });
});

function crearUsuario(){

  var baseurl = 'http://198.101.238.125:3000/usuarios';
  var usuarioid = document.getElementById("usuarioID").value;

  var correo = document.getElementById("emailID").value;

  if(usuarioid==""){
    alert("¡Usuario vacio!")
    return null
  }
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if(correo.match(mailformat))
  {
  //document.form1.text1.focus();

  }
  else
  {
  alert("¡Haz ingresado un correo electronico invalido!");
  //document.form1.text1.focus();
  return false;
  }


  var usuario ={
    "nombre" : usuarioid,
    "correo" : correo
  };

  console.log(JSON.stringify(usuario));

  fetch(baseurl , {
     method: 'POST',
     headers: {
    'Content-Type': 'application/json',
    'alg': 'HS256',
    'typ': 'JWT'
  },
     body: JSON.stringify(usuario)
  })
  .then(function(response) {
     if(response.ok) {
         return response.text()
     } else {
         throw "Error en la llamada Ajax";
     }

  })
  .then(function(texto) {
     console.log(texto);
  })
  .catch(function(err) {
     console.log(err);
  });
}
